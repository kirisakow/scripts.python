#!/usr/bin/python

"""
Exercice 8 : Nombre à deviner
1)  Ecrire un algorithme qui met en application le jeu suivant :
-  un nombre entier compris entre 1 et 100 est choisi par l’ordinateur de façon
aléatoire : ce sera le nombre que le joueur devra deviner ;
-  le joueur propose un entier et il lui est indiqué si le nombre proposé est égal,
supérieur ou inférieur au nombre à deviner ;
-  lorsque  le  nombre  à  deviner  a  été  trouvé,  il  sera  indiqué  le  nombre  de
tentatives faites par le joueur ;
-  le nombre de tentatives est illimité.
2)  Programmer en langage Python l’algorithme précédent.
"""

import random


class Messages:
    valeur_n = "Le nombre était {0}"
    proposez = "Proposez un entier positif compris entre {0} et {1} : "
    plus = "C'est PLUS que {0} !"
    moins = "C'est MOINS que {0} !"
    gagné = "C'est GAGNÉ ! " + valeur_n
    perdu = "C'est PERDU ! " + valeur_n


if __name__ == '__main__':
    print()

    borne_inf, borne_sup = (1, 100)
    nb_à_deviner = random.randint(borne_inf, borne_sup)
    nb_de_tentatives_max = 999
    nb_de_tentatives_utilisé = 0
    while nb_de_tentatives_utilisé <= nb_de_tentatives_max:
        nb_de_tentatives_utilisé += 1
        if nb_de_tentatives_utilisé == nb_de_tentatives_max:
            print(f"essai {nb_de_tentatives_utilisé} :",
                Messages.perdu.format(nb_à_deviner))
            break
        test = int(input(Messages.proposez.format(borne_inf, borne_sup)))
        if test == nb_à_deviner:
            print(f"essai {nb_de_tentatives_utilisé} :",
                Messages.gagné.format(test))
            break
        if test < nb_à_deviner:
            print(f"essai {nb_de_tentatives_utilisé} :",
                Messages.plus.format(test))
            continue
        if test > nb_à_deviner:
            print(f"essai {nb_de_tentatives_utilisé} :",
                Messages.moins.format(test))
            continue

    print()
