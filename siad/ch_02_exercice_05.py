#!/usr/bin/python

"""
Exercice 5: Catégorie
1) Ecrire  un  algorithme  qui  demande  l’âge  d’un  enfant  à  l’utilisateur.
Ensuite,  il l’informe de sa catégorie:
- "Poussin" de 6 à 7 ans
- "Pupille" de 8 à 9 ans
- "Minime" de 10 à 11 ans
- "Cadet" de 12 ans à 14 ans.
2) Programmer en langage Python l’algorithme précédent.
"""

import sys

categoriesAge = ("", "", "possin", "pupille", "minime", "cadet", "cadet")

agesCategorises = range(6, 14 + 1)


def get_categorie_dage(age):
    if age in agesCategorises:
        print("À l'âge de", age, "ans un enfant est un",
              categoriesAge[age // 2 - 1])
    elif age < min(agesCategorises):
        print("À l'âge de", age, "ans un enfant est encore trop jeune")
    elif age > max(agesCategorises):
        print("À l'âge de", age, "ans un enfant est déjà trop âgé")


if __name__ == '__main__':

    print()

    # age = int(sys.argv[1])

    for a in range(1, 18 + 1):
        get_categorie_dage(a)

    print()
