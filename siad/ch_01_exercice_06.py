#!/usr/bin/python

"""
Ecrire  un  algorithme  puis  le  code Python d’un programme  qui  lit  en  entrée
un capital  et  un  taux  annuel  de  placement  et  affiche  les  valeurs  successives
de  ce capital pendant les 10 premières années de placement:
    1.lorsque le placement est à intérêt simple;
    2.lorsque le placement est à intérêt composé.

Exemple de calculs (Source : https://www.moneyvox.fr/placement/interets.php)

Les intérêts sur le placement d'un capital de 100 € à un taux annuel de 5%
d'intérêts simples sur 2 ans :
     100 € × 5 % = 5 €
     Soit au total 5 € × 2 = 10 €.

Les intérêts sur le placement d'un capital de 100 € à un taux annuel de 5%
d'intérêts composés sur 2 ans :
    100 € × 5 % = 5 € la première année.
    (100 € + 5 €) × 5 % = 5,25 € la deuxième année.
    Soit au total 5 € + 5,25 € = 10,25 €.
"""

import sys

def convertTaux(taux):
    return taux if taux < 1 else taux / 100.0

def calculerInteretsSimples(capital, taux):
    taux = convertTaux(taux)
    return float(capital) * taux

def calculerInteretsComposes(capital, taux, nbAnnees):
    interets = 0
    for i in range(nbAnnees):
        interets += calculerInteretsSimples(capital, taux)
        capital += interets
    return round(interets, 2)


if __name__ == '__main__':

    capital = float(sys.argv[1])
    taux = convertTaux(float(sys.argv[2]))
    nbAnnees = int(sys.argv[3])

    print("           Le capital initial : " + str(capital) + " €")
    print("            Le taux d'intérêt : " + str(taux))
    print("           Le nombre d'années : " + str(nbAnnees))
    print("     Total d'intérêts simples : " +
          str(calculerInteretsSimples(capital, taux) * nbAnnees) + " €")
    print("    Total d'intérêts composés : " +
          str(calculerInteretsComposes(capital, taux, nbAnnees)) + " €")
