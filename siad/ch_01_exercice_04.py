#!/usr/bin/python

"""
Ecrire  un  algorithme  puis  le  coderPython  d’un programme  qui  demande
à l’utilisateur le rayon d’un cercle et qui affiche ensuite sa longueur et son aire.
"""

import sys
import math

def calculerLongueurDuCercle(r):
    return 2.0 * math.pi * r

def calculerAireDuCercle(r):
    return math.pi * r**2


if __name__ == '__main__':

    r = float(sys.argv[1])

    print("    La longueur du cercle : " + str(calculerLongueurDuCercle(r)))
    print("    L'aire du cercle : " + str(calculerAireDuCercle(r)))
