#!/usr/bin/python

"""
1.Rédiger  un  algorithme  permettant  de  calculer  le  pourcentage  de
  réduction d'un article connaissant le prix de départ et le prix à payer.
2.Coder en Python l’algorithme proposé.
"""

import sys

if __name__ == '__main__':

    prixSansRemise = float(sys.argv[1])
    prixAPayer = float(sys.argv[2])

    tauxDeRemise = abs(100 * (prixAPayer - prixSansRemise) / prixSansRemise)

    print("    Le prix sans remise : " + str(prixSansRemise) + " €")
    print("    Le prix à payer : " + str(prixAPayer) + " €")
    print("    Le taux de remise : " + str(tauxDeRemise) + " %")
