#!/usr/bin/python

"""
Exercice 1: Comparaison
1) Ecrire un algorithme qui demande deux nombres a et b de type réel ; affiche les
deux nombres saisis puis compare les deux nombres saisis.
2) Programmer en langage Python l’algorithme précédent.
"""

if __name__ == '__main__':

    nombre1 = int(input("Saisissez un nombre : "))
    nombre2 = int(input("Saisissez un autre nombre : "))
    if nombre1 != nombre2:
        print(str(max(nombre1, nombre2)) + " > " + str(min(nombre1, nombre2)))
    else:
        print(str(nombre1) + " = " + str(nombre2))
