#!/usr/bin/python

"""
Exercice 6: Etat de l’eau
1) Ecrire  un  algorithme  devant  donner  l’état  de  l’eau  selon  sa
température  doit pouvoir choisir entre trois réponses possibles (solide, liquide ou
gazeuse).
2) Programmer en langage Python l’algorithme précédent.
"""

import sys

tempCLow = 0
tempCHigh = 100
etats = ("solide", "liquide", "gazeux")

if __name__ == '__main__':

    print()

    userTemp = float(sys.argv[1])
    etat = ""

    if userTemp < tempCLow:
      etat = etats[0]
    elif tempCLow <= userTemp < tempCHigh:
      etat = etats[1]
    elif tempCHigh <= userTemp:
      etat = etats[2]

    print("État à", str(userTemp), "°C :", etat)

    print()
