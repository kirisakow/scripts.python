#!/usr/bin/python

"""
Exercice 3 : Année bissextile
Rappelons  qu’une  année  est  bissextile  tous  les  quatre  ans  sauf  lorsque
le millésime est divisible par 100 et non pas par 400 ; en d’autres termes, pour
qu’une année  soit  bissextile,  il  suffit  que  l’année  soit  un  nombre
divisible  par  4  et  non divisible par 100 ou alors divisible par 400.
1) Ecrire un algorithme qui demande à l’utilisateur de saisir une année et qui affiche si l’année saisie est bissextile ou non.
2) Programmer en langage Python l’algorithme précédent.
"""
print()

import sys
import math

if __name__ == '__main__':

    annee = int(sys.argv[1])

    print("L'année", annee, "est" if annee % 400 == 0 or (annee %
          4 == 0 and annee % 100 != 0) else "n'est pas", "bissextile.")

    print()
