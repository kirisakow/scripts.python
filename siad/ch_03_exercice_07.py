#!/usr/bin/python

"""
Exercice 7 : Nombres parfaits
Définition : Un nombre entier naturel est un nombre parfait s’il est égal à la somme de ses
diviseurs, 1 compris. Par exemple 6 est un nombre parfait car ses seuls diviseurs sont 1, 2 et
3 et on a : 6=1+2+3.
1)  Ecrire  l'algorithme  qui  demande  un  entier  naturel  n  non  nul
et  qui  affiche  les  n premiers nombres parfaits.
2)  Programmer en langage Python l’algorithme précédent.
"""

import sys


def getTousDiviseurs(n):
    res = [1]
    if n > 1:
        for i in range(2, n):
            if n % i == 0:
                res.append(i)
    return tuple(res)


if __name__ == '__main__':

    print()

    howManyPerfectNumbersToSearch = int(sys.argv[1])
    perfectNumbersList = []
    i = 1
    try:
        while len(perfectNumbersList) < howManyPerfectNumbersToSearch:
            print(str(i), end="\r")
            divs = getTousDiviseurs(i)
            if i == sum(divs):
                perfectNumbersList.append(i)
                print(str(i), "\t=", " + ".join(str(d) for d in divs))
            i += 1
    except KeyboardInterrupt as e:
        print(f'\r{i}')
    finally:
        print()