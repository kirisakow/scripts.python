#!/usr/bin/python

"""
1.Qu'affiche en sortie l'algorithme suivant ?
2.Modifier l'algorithme précédent pour que la valeur de x ne soit plus imposée mais soit
  saisie au départ.
3.Coder en Python l’algorithme proposé dans la question 2.
"""

import sys

if __name__ == '__main__':

    x = int(sys.argv[1])

    a = x - 1
    b = 3 * a
    c = b / 2
    d = c + 5

    print(d)
