#!/usr/bin/python

"""
Exercice 1 : Somme d’entiers
1)  Ecrire un algorithme qui lit en entrée un entier n et qui calcule et affiche la somme
des entiers impairs au plus égaux à n :
-  en utilisant une boucle « pour » ;
-  en utilisant une boucle « tant que ».
2)  Traduire en langage Python les algorithmes précédents.
"""

import sys

if __name__ == '__main__':

    print()

    input = int(sys.argv[1])
    somme = 0
    for i in range(input, 0, -1):
        if i % 2 == 1:
            somme += i
        i -= 1

    print("La somme des entiers impairs <= ", input, " vaut :", somme)

    print()
