#!/usr/bin/python

"""
Exercice 6 : Nombres de Armstrong
Définition : Un nombre entier naturel est un nombre de Armstrong si il est égal à la somme
des cubes des chiffres qui le composent. Par exemple, 153 est un nombre de Armstrong car
on a :
153 = 1^3 + 5^3 + 3^3
Mathématiquement, on démontre qu’il n’y a que quatre nombres de
Armstrong et qu’ils sont tous formés de trois chiffres.
1)  Ecrire un algorithme qui détermine tous les nombres de Armstrong.
2)  Programmer en langage Python l’algorithme précédent.
"""

if __name__ == '__main__':

    print()

    for i in range(100, 1000):
        if int(str(i)[0])**3 + int(str(i)[1])**3 + int(str(i)[2])**3 == i:
            print(i, "est un nombre d'Armstrong")

    print()
