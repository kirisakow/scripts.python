#!/usr/bin/python

"""
Nous disposons de
    billets de 100, 50 10 et 5 euros ;
    pièces de 2, 1 euros ;
    pièces de 50, 20, 10, 5, 2 et 1 centimes d’euros.

Le  but  de  l’exercice  est  de  rendre  la  monnaie  à  l’aide  de  ce  que  nous disposons.
On  suppose  que  nous  avons  suffisamment  de  pièces  et  de  billets  de chaque sorte pour
rendre la monnaie.

Ecrire un algorithme puis le code Python d’un programme qui indique la monnaie à rendre.
"""

# https://www.w3schools.com/python/python_classes.asp
# https://docs.python.org/2/library/fractions.html

import sys

# valeur en centimes
monnaie = {
    "billet(s) de 100 €" : 100*100,
    "billet(s) de 50 €" : 50*100,
    "billet(s) de 20 €" : 20*100,
    "billet(s) de 10 €" : 10*100,
    "billet(s) de 5 €" : 5*100,
    "pièces(s) de 2 €" : 2*100,
    "pièces(s) de 1 €" : 100,
    "pièces(s) de 0,50 €" : 50,
    "pièces(s) de 0,20 €" : 20,
    "pièces(s) de 0,10 €" : 10,
    "pièces(s) de 0,05 €" : 5,
    "pièces(s) de 0,02 €" : 2,
    "pièces(s) de 0,01 €" : 1
}

def calculerMonnaieARendreEtRetournerReste(montantEnCentimes, cleDuReferentiel, referentiel):
    nbDeBilletsOuPieces = montantEnCentimes // referentiel[cleDuReferentiel]
    if nbDeBilletsOuPieces > 0:
        print(i + "\tx " + str(nbDeBilletsOuPieces))
    return montantEnCentimes % referentiel[cleDuReferentiel]


if __name__ == '__main__':

    print("    Montant demandé (en centimes) : " + sys.argv[1])
    montantEnCentimes = int(sys.argv[1])

    for i in monnaie.keys():
        montantEnCentimes = calculerMonnaieARendreEtRetournerReste(
            montantEnCentimes, i, monnaie)
