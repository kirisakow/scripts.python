#!/usr/bin/python

"""
Ecrire un algorithme puis le code Python d’un programme qui lit
en entrée un angle exprimé en degré, le convertit en radian et
lui applique et affiche son sinus et son cosinus.
"""

import sys
import math

def convertDegToRad(a):
    return a / 180.0 * math.pi

def convertRadToDeg(a):
    return a * 180.0 / math.pi


if __name__ == '__main__':

    angleEnDeg = float(sys.argv[1])
    angleEnRad = convertDegToRad(angleEnDeg)

    print("           L'angle en degrés : " + str(angleEnDeg))
    print("    Le même angle en radians : " + str(angleEnRad))
    print("       Le sinus de cet angle : " + str(math.sin(angleEnRad)))
    print("     Le cosinus de cet angle : " + str(math.cos(angleEnRad)))
