#!/usr/bin/python

"""
Exercice 5 : Les grains de riz
Il était une fois un empereur hindou Chiram qui voulut récompenser l’un de ses
sujets  Seta  pour  son  invention  merveilleuse :  le  jeu  d’échecs.  « Comment  veux-tu
être récompensé ? » demanda Chiram ; « Donne-moi 2 grains de riz pour la première
case  de mon échiquier,  puis  4  grains  pour  la  seconde  case,  puis  8  grains  pour  la
troisième case et ainsi de suite jusqu’à la 64 ème  case » répondit Seta ; « accordé !!! »
lui répondit Chiram qui trouva ridicules ces quelques grains de riz.
1)  Ecrire un algorithme qui :
-  affiche le nombre de grains de riz sur chacune des 64 cases de l’échiquier ;
-  affiche le nombre total de grains de riz ;
-  affiche  la  masse  totale  en  tonnes  (on  supposera  qu’un  grain  de  riz  pèse  1
gramme).
2)  Programmer en langage Python l’algorithme précédent.
"""


class Grain:
    def __init__(self, label, weightInGrams):
        self.label = label
        self.weightInGrams = weightInGrams

    def getTotalMassOfNGrainsInTons(self, howManyGrains):
        return howManyGrains * self.weightInGrams / 1000


class Echiquier:
    def __init__(self, h, w):
        self.h = h
        self.w = w


def getHowManyGrainsInTotal(echiquier):
    howManyGrains = 0
    howManyCases = echiquier.w * echiquier.h
    for i in range(1, howManyCases + 1):
        howManyGrains += getHowManyGrainsOnNthCase(i)
    return howManyGrains


def getHowManyGrainsOnNthCase(n):
    return 2**n



if __name__ == '__main__':

    print()

    e = Echiquier(8, 8)
    print("Soit un échiquier " + str(e.w) + " x " + str(e.h))
    g = Grain("riz", 1)
    print("Soit un grain de " + g.label +
          " pesant " + str(g.weightInGrams) + " g")

    print("Nombre total de grains de riz sur l'échiquier :",
          '{:,d}'.format(getHowManyGrainsInTotal(e)))

    print("Poids total de grains de riz sur l'échiquier :",
          g.getTotalMassOfNGrainsInTons(getHowManyGrainsInTotal(e)))

    caseNumero = int(input("Entrez un numéro de case : "))

    print("Nombre de grains de riz sur la case numéro", caseNumero,
          ":", '{:,d}'.format(getHowManyGrainsOnNthCase(caseNumero)))

    print()
