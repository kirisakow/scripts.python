#!/usr/bin/python

"""
Exercice 3 : Moyenne
1)  Ecrire  un  algorithme  qui  lit  en  entrée  des  nombres  entiers  et  qui
affiche  la moyenne  de  ces  nombres  à  l'écran.  La  lecture  des  entiers
s'arrêtera  dès  lors qu'on lit l'entier 0, dont il ne sera pas tenu compte dans
le calcul de la moyenne, bien entendu.
2)  Programmer en langage Python l’algorithme précédent.
"""

print()

def demanderALUtilisateur():
    return int(input("Entrez un entier : "))


if __name__ == '__main__':

    somme = 0
    moyenne = 0
    compteur = 0

    while True:
        res = demanderALUtilisateur()
        if res != 0:
            somme += res
            compteur += 1
            moyenne = somme / compteur
        else:
            print("La moyenne des entiers lus est :", moyenne)
            break

    print()
