#!/usr/bin/python

"""
Exercice 4: Tarif
Un cinéma propose les tarifs suivants pour les groupes :
- 8 € la place pour les 5 premières ;
- 6 € la place pour les suivantes jusqu’à 10 ;
- 5,50 € la place au-delà de 10.
1) Le  responsable  d’une  association  vient  acheter  des  billets  ;
combien  devra-t-il payer pour 4 places ?
                          pour 9 places ?
                          pour 15 places ?
2) Écrire un algorithme permettant d’obtenir le montant à payer lorsque le
nombre de places est donné.
3) Le tester pour les valeurs de la question 1).
4) Le programmer en langage Python.
"""

import sys

if __name__ == '__main__':

    print()

    pax = int(sys.argv[1])

    tarifs = (8, 8, 8, 8, 8, 6, 6, 6, 6, 6)

    prixTotal = 0
    i = 0
    while i < pax:
        i += 1
        if i <= len(tarifs):
            prixTotal += tarifs[i - 1]
        else:
            prixTotal += (pax - len(tarifs)) * 5.5
            break

    print("Pour", pax, "personnes, le tarif est de", prixTotal, "€.")

    print()
