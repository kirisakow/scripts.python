#!/usr/bin/python

"""
Un commerçant accorde une remise sur des articles. On souhaite connaître le
montant de la remise en euros.
Voici un algorithme donnant la solution au problème :

1.Calculer la valeur de la variable r lorsque p= 56 et t= 30.
2.Même question avec p= 13 et t= 45.
3.Compléter l’algorithmepour afficherégalement le prix à payer c.
4.Calculer lavaleur des variables r et c lorsque p = 159 et t= 24.
5.Coder en Python l’algorithme de la question 3.
"""

import sys

prixSansRemise = float(sys.argv[1])
taux = float(sys.argv[2])

# calculer
def montantRemis(taux, prixSansRemise):
    return taux / 100 * prixSansRemise


def prixAPayer(prixSansRemise, montantRemis):
    return prixSansRemise - montantRemis


if __name__ == '__main__':
    montantRemis = montantRemis(taux, prixSansRemise)

    print()
    print("            Le prix : " + str(prixSansRemise) + " €")
    print("  Le taux de remise : " + str(taux) + " %")
    print("            ...soit : " + str(montantRemis) + " €")
    print("    Le prix à payer : " + str(prixSansRemise - montantRemis))
    print()
