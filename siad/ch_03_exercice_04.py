#!/usr/bin/python

"""
Exercice 4 : Factorielle
1)  Ecrire un algorithme qui lit en entrée un entier positif n et qui calcule et affiche la
factorielle de l’entier n :
-  en utilisant une boucle « tant que » ;
-  en utilisant une boucle « pour ».
2)  Programmer en langage Python l’algorithme précédent.
Rappel : on rappelle que : n! = n(n-1)*…*1 si n > 0 et que 0! = 1
"""

print()

def demanderALUtilisateur():
    return int(input("Entrez un entier : "))

def calculerFactorielleAvecFor(n):
    res = n
    if n == 0:
        res = 0
    elif n == 1:
        res = 1
    else:
        for i in range(n - 1, 0, -1):
            res *= i
    return res

def calculerFactorielleAvecWhile(n):
    res = n
    if n == 0:
        res = 0
    elif n == 1:
        res = 1
    else:
        i = n - 1
        while i > 0:
            res *= i
            i -= 1
    return res


if __name__ == '__main__':

    entierSaisi = demanderALUtilisateur()
    print("La factorielle " + str(entierSaisi) + "! = " +
          str(calculerFactorielleAvecFor(entierSaisi)))
    print("La factorielle " + str(entierSaisi) + "! = " +
          str(calculerFactorielleAvecWhile(entierSaisi)))
