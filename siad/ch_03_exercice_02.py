#!/usr/bin/python

"""
Exercice 2 : Somme d’entiers saisis « à la volée »
1)  Ecrire un algorithme qui lit au clavier des entiers et qui calcule et affiche leur somme. La
lecture des entiers s'arrêtera dès lors que l'entier 0 est lu.
    Exemple d’affichage désiré :
            Entrez un entier : 6
            Entrez un entier : -1
            Entrez un entier : 5
            Entrez un entier : 0
            La somme des entiers lus est 10

2)  Traduire en langage Python l’algorithme précédent.
"""


def demanderALUtilisateur():
    return int(input("Entrez un entier : "))



if __name__ == '__main__':

    print()

    somme = 0

    while True:
        res = demanderALUtilisateur()
        if res != 0:
            somme += res
        else:
            print("La somme des entiers lus est :", somme)
            break

    print()
