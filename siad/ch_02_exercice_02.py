#!/usr/bin/python

"""
Exercice 2 : Résolution dans IR de l’équation : ax²+bx+c=0
1) Ecrire un algorithme qui :
 * demande trois nombres réels a, b et c;
 * donne les solutions de l’équation ax²+bx+c=0.
2) Programmer en langage Python l’algorithme précédent.
Remarques :
- penser à envisager tous les cas possibles suivant les valeurs de a, b et c ;
- pour le calcul d’une racine carrée on peut soit utiliser la puissance 0.5 soit utiliser la fonction racine carrée sqrt(). Dans ce
  dernier cas il  faudra  importer  cette fonction de la bibliothèque math à l’aide de l’instruction : from math import sqrt
"""
print()

import sys
import math

if __name__ == '__main__':

    a = float(sys.argv[1])
    b = float(sys.argv[2])
    c = float(sys.argv[3])
    equation = str(a) + "x²" + ("+" if b > 0 else "") + \
        str(b) + ("x+" if c > 0 else "x") + str(c)
    print("    L'équation : " + equation)

    if a == 0:
        print("    x = " + str(-c / b))
    else:
        delta = b**2 - 4*a*c
        print("    Le delta = " + str(delta))
        if delta < 0:
            print("    Pas de racine pour l'équation " + equation)
        elif delta == 0:
            x = -b / 2*a
            print("    x = " + str(x))
        else:
            x1 = (-b - math.sqrt(delta)) / 2*a
            x2 = (-b + math.sqrt(delta)) / 2*a
            print("    x1 = " + str(x1))
            print("    x2 = " + str(x2))

    print()
