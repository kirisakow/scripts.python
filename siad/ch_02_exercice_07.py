#!/usr/bin/python

"""
Exercice 7: Mois en toutes lettres
1) Écrire  un  algorithme  permettant  d'afficher  le  mois  en  toutes  lettres
selon  son numéro saisi au clavier.
2) Programmer en langage Python l’algorithme précédent.
"""


import sys

mois = (
    "janvier", "février",
    "mars", "avril", "mai",
    "juin", "juillet", "août",
    "septembre", "octobre", "novembre",
    "décembre"
)

if __name__ == '__main__':

    print()

    moisNumero = int(sys.argv[1])

    print("Le mois n°", moisNumero, ("est " +
          mois[moisNumero - 1]) if moisNumero < len(mois) else ("n'existe pas"))

    print()
