#!/usr/bin/python

"""
Exercice 2 :
1)  Un diviseur de n qui est différent de n est appelé diviseur strict de n.
Ecrire un algorithme qui crée un tableau contenant tous les diviseurs stricts de n.
2)  Un nombre abondant est un nombre dont la somme de ses diviseurs stricts est
supérieure  au  nombre.  Ecrire  un  algorithme  qui  affiche  tous  les  nombres
abondants inférieurs à 100.
3)  Un nombre parfait est un nombre dont la somme de ses diviseurs stricts est égale
au nombre. Ecrire un algorithme qui affiche tous les nombres parfaits inférieurs à
1000.
4)  Deux  nombres  n  et  m  sont amicaux  si  la  somme des  diviseurs  stricts
de  n est égale  à  m,  et  la  somme  des  diviseurs  stricts  de  m  est  égale
à  n.  Ecrire  un algorithme qui affiche tous les couples de nombres amicaux
inférieurs à 5000.
5)  Traduire en langage Python les différents algorithmes des questions précédentes.
"""

print()

import math

def getTousDiviseursStricts(n):
    res = [1]
    for i in range(2, n):
        if n % i == 0:
            res.append(i)
    return tuple(res)

def isNombreParfait(n):
    return sum(getTousDiviseursStricts(n)) == n

def isNombreAbondant(n):
    return sum(getTousDiviseursStricts(n)) > n

def getTousNombresAbondantsJusquA(limite):
    res = []
    for i in range(1, limite):
        if isNombreAbondant(i):
            res.append(i)
    return tuple(res)


if __name__ == '__main__':
    limite = 100
    nbAbondants = getTousNombresAbondantsJusquA(limite)
    print("Nombres abondants jusqu'à", limite, ":",
          " ".join(str(i) for i in nbAbondants))

def getTousNombresParfaitsJusquA(limite):
    res = []
    for i in range(1, limite):
        if isNombreParfait(i):
            res.append(i)
    return tuple(res)


if __name__ == '__main__':
    limite = 1000
    nbParfaits = getTousNombresParfaitsJusquA(limite)
    print("Nombres parfaits jusqu'à", limite, ":",
          " ".join(str(i) for i in nbParfaits))

def isCoupleNombresAmicaux(n, m):
    return sum(getTousDiviseursStricts(n)) == m and sum(getTousDiviseursStricts(m)) == n

def getTousNombresAmicauxJusquA(limite):
    res = []
    for n in range(1, limite):
        for m in range(n + 1, limite):
            if isCoupleNombresAmicaux(n, m):
                res.append((n, m))
            print(n, ",", m, end="\r")
    return tuple(res)


if __name__ == '__main__':
    limite = 5000
    nbAmicaux = getTousNombresAmicauxJusquA(limite)
    print("Nombres amicaux jusqu'à", limite, ":",
          " ".join(str(i) for i in nbAmicaux))

print()