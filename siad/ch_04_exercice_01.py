#!/usr/bin/python

"""
Exercice 1 : Somme d’entiers
1.  Ecrire un algorithme qui :
  - remplit un tableau composé de cinq nombres entiers demandés à l’utilisateur ;
  - donne la valeur maximale et minimale des valeurs composant le tableau ;
  - donne la moyenne des valeurs composant le tableau.
2.  Traduire en langage Python les algorithmes précédents.
"""

if __name__ == '__main__':

    print()

    tableau = []
    tableauLen = 5
    for i in range(1, tableauLen + 1):
        n = int(input("Donnez un nombre : "))
        tableau.append(n)

    nbMin = tableau[0]
    nbMax = tableau[0]
    for i in range(1, len(tableau)):
        if tableau[i] < nbMin:
            nbMin = tableau[i]
        elif tableau[i] > nbMax:
            nbMax = tableau[i]
    print("Le minimum =", nbMin)
    print("Le maximum =", nbMax)
    print("La moyenne =", round(sum(tableau)/len(tableau), 2))

    print()
