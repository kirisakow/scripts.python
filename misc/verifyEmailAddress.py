"""
A simple script for email address verification using syntax, DNS and mailbox verification

Source: https://github.com/scottbrady91/Python-Email-Verification-Script

Accompanying article available on: https://www.scottbrady91.com/Email-Verification/Python-Email-Verification-Script
"""

import colorama
import sys
import re
import smtplib
import dns.resolver

# Address used for SMTP MAIL FROM command
fromAddress = 'corn@bt.com'

# Simple Regex for syntax checking
regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$'

# Email address to verify
inputAddress = sys.argv[1]
# inputAddress = input('Please enter the emailAddress to verify:')
addressToVerify = str(inputAddress)

# Syntax check
match = re.match(regex, addressToVerify, re.IGNORECASE)
if match == None:
    print('Bad Syntax')
    raise ValueError('Bad Syntax')

# Get domain for DNS lookup
splitAddress = addressToVerify.split('@')
domain = str(splitAddress[1])
print(f"Domain: {domain}")

# MX record lookup
records = dns.resolver.resolve(domain, 'MX')
mxRecord = records[0].exchange
mxRecord = str(mxRecord)


# SMTP lib setup (use debug level for full output)
server = smtplib.SMTP()
server.set_debuglevel(0)

# SMTP Conversation
server.connect(mxRecord)
# server.local_hostname(Get local server hostname)
server.helo(server.local_hostname)
server.mail(fromAddress)
code, message = server.rcpt(str(addressToVerify))
server.quit()

# print(f"HTTP Response code: {code}")
# print(f"HTTP Response message: {message}")

# Assume SMTP response 250 is success
if code == 250:
    print(f"{colorama.Fore.GREEN}\N{check mark}{colorama.Style.RESET_ALL} Success: the address {inputAddress!r} exists")
else:
    print(f"{colorama.Fore.RED}\N{ballot x}{colorama.Style.RESET_ALL} Fail: the address {inputAddress!r} does not exist")
