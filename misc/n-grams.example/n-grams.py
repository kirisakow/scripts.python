#!/usr/bin/python3
#
# Usage:
# python ngrams.py /path/to/textfile minGrams maxGrams | sort -bnr
#
# With the “sort -bnr” command meaning:
# * -b: ignore leading blanks
# * -n: compare according to string numerical value
# * -r: reverse the result of comparisons
#
# -*- coding: utf-8 -*-
from nltk.util import ngrams
import re
import string
import sys


def lireFichier(fichier):
    with open(fichier, encoding='utf-8', errors='ignore') as f:
        content = f.readlines()
    return content


contenu = lireFichier(sys.argv[1])

dictionnaire = dict()

minGrams = int(sys.argv[2])
maxGrams = int(sys.argv[3])

for n in range(minGrams, maxGrams):
    for ligne in contenu:
        ligne = re.sub(
            r"[\\!\"#$%&()*+,./:;<=>?@[\]^_`{|}~–—«»“”]", " ", ligne)
        ligne = re.sub(r" ['-]|['-] ", " ", ligne)
        nngrams = ngrams(ligne.split(), n)
        for grams in nngrams:
            phrase = " ".join(grams)
            if phrase in dictionnaire:
                dictionnaire[phrase] += 1
            else:
                dictionnaire[phrase] = 1

for phrase in dictionnaire:
    freq = dictionnaire[phrase]
    if freq > 1:
        print(str(freq).rjust(7), phrase, sep=',')
