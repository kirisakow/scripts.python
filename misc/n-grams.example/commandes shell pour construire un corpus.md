# Build a corpus from a Telegram chat

## 1. Download as HTML pages

## 2. Extract text from HTML with pup

2.1. Dates:
```
cat $(ls -rt1b *.html) | pup 'div[class="body"] div[class="pull_right date details"] attr{title}' > date.txt
```
2.2. Authors:
```
cat $(ls -rt1b *.html) | pup 'div[class="body"] div[class="from_name"] text{}' > author.txt
```
2.3. Any messages:
```
cat $(ls -rt1b *.html) | pup 'div[class="body"] json{}' > messages-full.json
```
2.4. Only text messages:
```
cat $(ls -rt1b *.html) | pup 'div[class="body"] div[class="text"] text{}'
```
or:
```
cat $(ls -rt1b *.html) | pup 'div[class="body"]:parent-of(div[class="text"]) json{}' > messages-text-full.json
```
2.5. Clean up by appending a comma behind a `]` on otherwise empty lines:
```
jq '.. | .[].children' messages-text-full.json | sed 's/^]$/],/g' > messages-text-full.clean.json
```
2.6. Manually put content in brackets:
* append an opening bracket at the beginning of the file;
* replace the end comma by a closing bracket.

## 3. Extract from JSON with jq all date-time strings

3.1. ...of all messages:
```
jq '.. | .[].children | .[] | select(.class == "pull_right date details") | .title' messages-full.json > messages-all-date
```
3.2. ...only of text messages (with deleting of empty lines; not necessary if JSON has been cleaned up):
```
jq '.. | .[].children | if (last(.[]) | .class) == "text" then (.[] | select(.class == "pull_right date details") | .title) else "" end' messages-full.json | sed '/^""$/d' > messages-text-date
```

## 4. Extract from JSON with jq all author strings

4.1. ...of all messages:
```
jq '.. | .[].children | .[] | select(.class == "from_name") | .text' messages-full.json > messages-all-from_name
```

4.2. ...only of text messages (with deleting of empty lines; not necessary if JSON has been cleaned up):

```
jq '.. | .[].children | if (last(.[]) | .class) == "text" then (.[] | select(.class == "from_name") | .text) else "" end' messages-full.json | sed '/^""$/d' > messages-text-from_name
```

## 5. Extract all message content strings
5.1. ...from a yet uncleaned file
```
jq '.. | .[].children | .[] | select(.class == "text") | .text' messages-full.json > messages-text-content
```
5.1. ...from an already cleaned file
```
jq '.. | .[] | .[] | select(.class == "text") | if (.text != null) then .text else .children[].text end' messages-text-full.json > messages-text-content
```

## 6. Concatenate message dates with contents
```
paste -d ',' messages-text-date messages-text-content > messages-text-date+messages-text-content.csv
```
## 7. Manually insert a header into the CSV file.

## 8. Remove null text messages
```
sed '/,null/d' messages-text-date+messages-text-content.csv
```

## 9. Look up for any `&#xxx;` encoded characters
Find all (distinct) occurrencies of encoded characters so that you know if you need to perform an additional cleaning:
```
grep -oE '&(#[0-9]+|[a-z]+);' messages-text-content | sort | uniq
```

## 10. Build N-grams with a Python script called with AWK:
```
awk 'BEGIN { 
  system("date +\"%F %T.%3N\""); 
  print("\r Begin generating N-grams...");
  for (i = 10; i >= 1; i--) {
    system("python ngrams.py messages-text-content " i " " i+1 " | sort -bnr > grams." i); 
  }
  print("\r End generating N-grams...");
  system("date +\"%F %T.%3N\"")
}'
```

## 11. Look up N-grams for a specific string
```
cat $(ls -t1b grams.*) | grep -Ei 'vot[ée].* pour' | sort -bnr
```
