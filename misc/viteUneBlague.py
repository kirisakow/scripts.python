#!/usr/bin/python3
import asyncio
import bs4
import numpy as np
import re
import requests
import sys


async def get_soup(url: str) -> bs4.BeautifulSoup:
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'}
    response = requests.get(url, headers=headers)
    html_content = response.text
    soup = bs4.BeautifulSoup(html_content, features='html5lib')
    return soup


async def how_many_pages(home_page_soup: bs4.BeautifulSoup) -> int | None:
    """Tells how many pages the blog has, from analyzing home page contents."""
    max_pages = None
    pagination_labels = re.findall(
        pattern=r'Page 1 sur (\d+) pages\.',
        string=home_page_soup.select_one('div.post-content.entry-content').text
    )
    if len(pagination_labels) == 2 \
            and pagination_labels[0] == pagination_labels[1]:
        max_pages = int(pagination_labels[0])
    return max_pages


async def prepare_content():
    """The outer (wrapper) async function, to be called by the event loop from
    a normal function"""
    base_url = ('https://dicocitations.lemonde.fr/blague-du-jour', '.php')
    url0 = ''.join(base_url)
    soup0 = await get_soup(url0)
    max_nb_pages = await how_many_pages(soup0)
    if max_nb_pages is None:
        sys.exit("Erreur lors de la récupération des données.")
    random_page_index = np.random.randint(max_nb_pages)
    if random_page_index == 0:
        soup = soup0
    else:
        url = f'{base_url[0]}{-random_page_index}{base_url[1]}'
        soup = await get_soup(url)
    blagues_titres = soup0.select('a > h2 > div')
    blagues_contenu = soup0.select('a ~ font')
    how_many_jokes_on_page = 0
    if len(blagues_titres) == len(blagues_contenu):
        how_many_jokes_on_page = len(blagues_titres)
    random_joke_index = np.random.randint(how_many_jokes_on_page - 1)
    return (
        blagues_titres[random_joke_index].text.upper(),
        blagues_contenu[random_joke_index].text
    )


def viteUneBlague():
    write, flush = sys.stdout.write, sys.stdout.flush
    # clear terminal with an escape sequence
    write(chr(27) + '[2J')
    flush()
    loop = asyncio.get_event_loop()
    ready_result = loop.run_until_complete(
        prepare_content()
    )
    loop.close()
    write('\n'.join(ready_result))
    flush()


if __name__ == '__main__':
    viteUneBlague()
