#!/usr/bin/python

# curl -s https://dicocitations.lemonde.fr/blague-du-jour.php \
#   | pup 'font:nth-of-type(1) json{}' \
#     | jq -r '.[1].children | map(select(.tag == "p").text) | .[]'

import json
import random
import re
import subprocess
import sys

# clear terminal with an escape sequence
print(chr(27) + "[2J")

# url
baseUrl = ("https://dicocitations.lemonde.fr/blague-du-jour", ".php")


def buildUrl(url, whichPage=0):
    if whichPage == 0:
        url = "".join(str(d) for d in url)
    else:
        sep = "-" + str(whichPage) if whichPage > 0 else ""
        url = sep.join(str(d) for d in url)
    return url


def getNbPagesMax(url):
    p1 = subprocess.Popen(
        ["curl", "-s", url], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["pup", "div#content text{}"],
                          stdin=p1.stdout, stdout=subprocess.PIPE)
    p3 = subprocess.Popen(["grep", "Page 1 sur"],
                          stdin=p2.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    output, err = p3.communicate()
    nbPagesMax = output.split("\n".encode(
        "utf-8"))[0].split(" ".encode("utf-8"))[3]
    return int(nbPagesMax)


LAST_POSSIBLE_PAGE = getNbPagesMax(
    buildUrl(baseUrl)
)


def getNbJokesMaxOverall():
    pass


def getNbJokesOnPage(whichPage=0):
    p1 = subprocess.Popen(
        ["curl", "-s", buildUrl(baseUrl, whichPage)],
        stdout=subprocess.PIPE
    )
    p2 = subprocess.Popen(
        ["pup",
         "font[color=\"black\"][size=\"3\"] json{}"],
        stdin=p1.stdout, stdout=subprocess.PIPE
    )
    p3 = subprocess.Popen(
        ["jq", "-r", ". | length"],
        stdin=p2.stdout, stdout=subprocess.PIPE
    )
    p1.stdout.close()
    p2.stdout.close()
    output, err = p3.communicate()
    return int(output)

def unfold_json(dict_or_list):
    ret = []
    if isinstance(dict_or_list, list):
        for ch in dict_or_list:
            ret.append(
                unfold_json(ch)
            )
    if isinstance(dict_or_list, dict):
        if 'text' in dict_or_list.keys():
            ret.append(
                dict_or_list['text']
            )
        elif 'children' in dict_or_list.keys():
            ret.append(
                unfold_json(
                    dict_or_list['children']
                )
            )
    return '\n'.join(ret)

def getJoke(url, whichPage, whichJoke):
    p1 = subprocess.Popen(
        ["curl", "-s", url], stdout=subprocess.PIPE
    )
    p2 = subprocess.Popen(
        ["pup",
         "font[color=\"black\"][size=\"3\"]:nth-of-type(" + str(whichJoke) + ") json{}"],
        stdin=p1.stdout, stdout=subprocess.PIPE
    )
    p1.stdout.close()
    output, err = p2.communicate()
    output = json.loads(output.decode('utf-8'))
    return unfold_json(output)


def tryPage(numbers):
    sys.stdout.write(".")
    sys.stdout.flush()
    p = numbers.pop()
    n = getNbJokesOnPage(p)
    if n > 0:
        return p, n
    else:
        if numbers:
            tryPage(numbers)
        else:
            return '', '', ''


def getRandomJoke():
    tryPageRes = None
    while tryPageRes is None:
        tryPageRes = tryPage(
            set(
                range(LAST_POSSIBLE_PAGE)
            )
        )
    if len(tryPageRes) == 3:
        return
    elif len(tryPageRes) == 2:
        whichPage = tryPageRes[0]
        nbJokesOnPage = tryPageRes[1]
        whichJoke = random.randrange(1, stop=nbJokesOnPage)
        joke = getJoke(
            buildUrl(baseUrl, whichPage),
            whichPage,
            whichJoke
        )
        if joke.strip() != "":
            sys.stdout.write("\r")
            print(joke)
            return
        else:
            getRandomJoke()


def searchForJokes(regexPattern, maxResults=1):
    searchResCounter = 0
    for whichPage in range(LAST_POSSIBLE_PAGE):
        tryPageRes = None
        while tryPageRes is None:
            tryPageRes = tryPage(set([whichPage]))
        if len(tryPageRes) == 3:
            pass
        elif len(tryPageRes) == 2:
            nbJokesOnPage = tryPageRes[1]
            for whichJoke in range(1, nbJokesOnPage):
                joke = getJoke(
                    buildUrl(baseUrl, whichPage),
                    whichPage,
                    whichJoke
                )
                if joke.strip() != "":
                    if re.search(rf"{re.escape(regexPattern)}", joke, re.IGNORECASE | re.MULTILINE | re.DOTALL) is not None:
                        searchResCounter += 1
                        sys.stdout.write("\r")
                        if maxResults > 1:
                            print(searchResCounter, "/", maxResults, "\n")
                        print(joke)
                        if searchResCounter == maxResults:
                            return

# get rid of the first arg to stay logical
sys.argv.pop(0)
# if no args are passed: just get a random joke
if not sys.argv or sys.argv[0].strip() == "":
    sys.stdout.write("Minute, papillon")
    sys.stdout.flush()
    getRandomJoke()
# if at least one non-empty arg is passed: 
else:
    # search for a joke that matches the given regex pattern
    regexPattern = sys.argv[0]
    maxResults = 1
    if len(sys.argv) == 2 \
        and sys.argv[1].strip() != "" \
        and sys.argv[1].isnumeric() \
        and int(sys.argv[1]) != 0:
        maxResults = int(sys.argv[1])
    searchForJokes(
        regexPattern=regexPattern,
        maxResults=maxResults
    )
