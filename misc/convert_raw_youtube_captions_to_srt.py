"""
Convert a JSON file with raw YouTube captions to valid SubRip file (.srt)

Requirements:

* Python 3
* jq — https://stedolan.github.io/jq/download/

Usage:
1. Download YouTube captions in JSON format to the same directory that this script
2. Run in terminal the following command:

cat ./subtitles.ru.json | jq '.actions[0].updateEngagementPanelAction.content.transcriptRenderer.body.transcriptBodyRenderer.cueGroups' | jq --raw-output '.[].transcriptCueGroupRenderer.cues[].transcriptCueRenderer | if .cue != {} then "\((map(.)|index(.)|tonumber)+1)\n\(.startOffsetMs) --> \((.startOffsetMs|tonumber) + (.durationMs|tonumber))", "\(.cue.simpleText)\n" else "" end' | python3 convert_raw_youtube_captions_to_srt.py | tee ./subtitles.ru.srt

"""

import sys
import re
from datetime import timedelta
import io


def toSrtFormat(timeDeltaObject):
    parts = str(timeDeltaObject).split(':')
    str_hours = parts[0].zfill(2)
    str_minutes = parts[1].zfill(2)
    str_seconds = ''
    str_milliseconds = ''
    hasDotAndMillis = re.search(r'\.', parts[2])
    if hasDotAndMillis:
        str_seconds = parts[2].split('.')[0].zfill(2)
        str_milliseconds = parts[2].split('.')[1][0:3]
    else:
        str_seconds = parts[2].zfill(2)
        str_milliseconds = '000'
    return f"{str_hours}:{str_minutes}:{str_seconds},{str_milliseconds}"


regex_timestamp = r"^([0-9]+) --> ([0-9]+)$"
regex_index = r"^1.?$"
index_counter = 0

for line in sys.stdin:
    matches_index = re.search(regex_index, line, re.DOTALL)
    matches_timestamp = re.search(regex_timestamp, line)
    if matches_index:
        index_counter += 1
        print(
            str(index_counter)
        )
    elif matches_timestamp:
        millis_from = int(matches_timestamp.group(1))
        millis_to = int(matches_timestamp.group(2))
        timestamp_from = timedelta(milliseconds=millis_from)
        timestamp_from = toSrtFormat(timestamp_from)
        timestamp_to = timedelta(milliseconds=millis_to)
        timestamp_to = toSrtFormat(timestamp_to)
        print(
            f"{timestamp_from} --> {timestamp_to}"
        )
    else:
        print(line, end='')
